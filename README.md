# Replayed Player

[![Support me on Ko-Fi](https://img.shields.io/static/v1?label=Support%20me%20on&message=Ko-fi&color=FF5E5B&logo=kofi&style=for-the-badge)](https://ko-fi.com/jolozs)

A dead simple video player with nice progress UI. Intended for use on Live Streams to play videos without issues (hopefully). Heavily inspired by the player used in the [Mental Checkpoint](https://www.youtube.com/MentalCheckpoint) Replay Value streams.

![Replayed playing "The Wolf" by SIAMES](https://totallynotavir.us/i/yeuzwo7x.png)
Video: [The Wolf by SIAMES](https://www.youtube.com/watch?v=lX44CAz-JhU)

## Installation

The installation of Replayed is easy. Simply clone this repository and run `npm i` inside the cloned folder.

## Usage

You can run the app by executing the command `npm start`. It'll prompt you to select a video file which will get played. A window will open without the player playing. You can now stream this window and press `Ctrl+Space` to play the video. This shortcut can be customized using the config below. Shortcuts work system wide, so even if you got the window in the background it will trigger the action.

## Configuration

In the root of the folder, there is a `config.json` file. It houses all configuration needed by the player. **Every option must be present in the config file or it won't work.** Names with a dot in them (eg. `buttons.forward`) exist in arrays with the part beforw the dot being the array name and the part after the actual property name. All button inputs use [Electrons Accelerators](https://www.electronjs.org/docs/latest/api/accelerator).

| Name               | Default Value              | Explaination                                                                                       |
| ------------------ | -------------------------- | -------------------------------------------------------------------------------------------------- |
| `seekSeconds`      | `5`                        | The amount of seconds the video skips when you fast forward/backward.                              |
| `buttons.forward`  | `"CommandOrControl+Right"` | The button (combination) you press when you want to fast forward.                                  |
| `buttons.backward` | `"CommandOrControl+Left"`  | The button (combination) you press when you want to go backward.                                   |
| `playback.height`  | `1080`                     | The number of pixels your video is high.                                                           |
| `playback.width`   | `1920`                     | The number of pixels your video is wide.                                                           |
| `playback.scale`   | `0.5`                      | The height and width will be multiplied by this, used to make the output window bigger or smaller. |

### Default `config.json`

```json
{
  "seekSeconds": 5,
  "buttons": {
    "forward": "CommandOrControl+Right",
    "backward": "CommandOrControl+Left",
    "pause": "CommandOrControl+Space"
  },
  "playback": {
    "height": 1080,
    "width": 1920,
    "scale": 0.5
  }
}
```

## Note about the UI

The UI is just there for decoration and not actually interactive.
