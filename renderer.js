var { ipcRenderer } = require("electron");

var video = document.getElementById("mainVideo");
var bar = document.getElementById("bar");
var playIcon = document.getElementById("play");
var pauseIcon = document.getElementById("pause");

setInterval(() => {
  var perc = video.currentTime / video.duration;

  bar.style.width = perc * 100 + "%";
}, 10);

video.addEventListener("play", () => {
  playIcon.style.display = "none";
  pauseIcon.style.display = "block";
});
video.addEventListener("pause", () => {
  playIcon.style.display = "block";
  pauseIcon.style.display = "none";
});

ipcRenderer.on("pause", () => {
  video.pause();
});
ipcRenderer.on("play_toggle", () => {
  console.log("Got request to toggle playback");
  if (
    !(
      video.currentTime > 0 &&
      !video.paused &&
      !video.ended &&
      video.readyState > 2
    )
  ) {
    video.play();
  } else {
    video.pause();
  }
});

ipcRenderer.on("seek_dynamic", (e, duration) => {
  console.log("Got dynamic seek request for", duration);
  video.currentTime += duration;
});

var videoPath = ipcRenderer.sendSync("video");

video.src= videoPath;