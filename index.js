const {
  app,
  BrowserWindow,
  ipcMain,
  globalShortcut,
  dialog,
} = require("electron");
const path = require("path");
var config = require("./config.json");

const createWindow = () => {
  var file = dialog.showOpenDialogSync({
    title: "Select a video file",
    buttonLabel: "Play",
  })[0];

  console.log("Selected video", file);

  if (file == undefined) {
    app.quit();
  }

  const mainWindow = new BrowserWindow({
    width: config.playback.width * config.playback.scale,
    height: config.playback.height * config.playback.scale,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
    frame: false,
    resizable: false,
  });

  mainWindow.loadFile("index.html");

  globalShortcut.register(config.buttons.pause, () => {
    console.log("Trying to toggle playback");
    mainWindow.webContents.send("play_toggle");
  });

  globalShortcut.register(config.buttons.backward, () => {
    console.log("Trying to seek", config.seekSeconds * -1, "seconds");
    mainWindow.webContents.send("seek_dynamic", config.seekSeconds * -1);
  });
  globalShortcut.register(config.buttons.forward, () => {
    console.log("Trying to seek", config.seekSeconds, "seconds");
    mainWindow.webContents.send("seek_dynamic", config.seekSeconds);
  });

  ipcMain.on("video", (e) => {
    e.returnValue = file;
  });
};

app.whenReady().then(() => {
  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});
